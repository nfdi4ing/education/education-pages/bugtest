## 2. Definition: Was ist ein DMP?

* Systematische Beschreibung des Umgangs mit (Forschungs)daten während des gesamten Projektverlaufs und darüber hinaus
* Beim Projektbeginn erstellte Sammlung der erwarteten Daten und Zuständigkeiten
* Wird über Projektverlauf aktuell gehalten
* Gewährleistet Nachvollziehbarkeit und Übersichtlichkeit


## Was ist ein DMP? II

* Instrument zur effizienten Organisation der Daten innerhalb eines Forschungsvorhabens (projekt-spezifisch)
* Gewähr für die Nachvollziehbarkeit und 
* Interpretierbarkeit der Forschungsergebnisse (für die eigene Projektarbeit und für Dritte) 
* Dynamisches, lebendes Dokument, das im Laufe eines Projekts aktualisiert werden sollte


## Was ist ein DMP? III

* Angaben zu Projektleitung, Projekttitel, Projektbeschreibung, Förderorganisation, Art und Umfang der entstandenen Forschungsdaten  Speicherung, Aufbewahrungsdauer usw. 
* Ermöglichung der Interpretation, Nachvollziehbarkeit und Nachnutzung von Forschungsergebnissen in späteren Jahren
* Leitfaden für den strukturierten Umgang mit Forschungsdaten während des Projektverlaufs und darüber hinaus

