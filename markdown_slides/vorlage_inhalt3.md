## 3. Zweck: Was kann ein DMP? 

* Erfüllung von Anforderungen der Fördergeber (derzeit noch)
* Kommunikationstool zwischen Projektpartnern / Single Point of Information
* Selbstmanagement / projektinternes FDM:
* Welche Daten werden erwartet?
* Welche fallen tatsächlich an?


## Was kann ein DMP? II

* Vorbereitung der Nach- und Weiternutzung:
* Bei Projektabschluss bleiben Daten zugänglich und verständlich
* Nachnutzbarkeit wird gewährleistet
* Für Andere (z.B. Kollegen / Vorgesetzte) sind die Daten leichter zugänglich
* Weniger Arbeit beim Einarbeiten Anderer in das Projekt


![](https://git.rwth-aachen.de/nfdi4ing/education/education-pages/revealjs/-/raw/master/media_files/image.png "Title")



## 4. Nutzen: Welche Vorteile bietet ein DMP?

* Steigerung der Qualität und Effizienz der Forschungsarbeit
* Verhinderung von Doppelarbeit durch bessere Organisation
* Es existieren bereits Vorlagen für DMPs, sodass Forschende sich wenig Gedanken über die Gestaltung machen müssen
* Einfache Nutzung von bereits erarbeiteten Ergebnissen

