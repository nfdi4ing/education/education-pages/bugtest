## 1. Ziele der Übung

Zu vermittelndes Wissen und Inhalte

* Grundlagen zum Datenmanagementplan
* Definition
* Zweck
* Nutzen für IngenieurInnen
* Use-Cases mit praktischen Beispielen
* Hands-On Übung mit dem DMP-Tool RDMO (Research Data Management Organizer)
